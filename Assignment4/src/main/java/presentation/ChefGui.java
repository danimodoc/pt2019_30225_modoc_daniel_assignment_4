package presentation;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ChefGui extends JFrame implements Observer {
	
	JPanel content = new JPanel();
	JPanel content1 = new JPanel();
	
	JLabel label = new JLabel();
	
	public ChefGui() {
		
		content.add(content1);
		
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 300);

		this.setContentPane(content);
		// f.pack();
		this.setTitle("Chef Message");
		//this.setVisible(true);
	}
	
	
    public void update(Observable o, Object data) {	
        label.setText((String)data);
        content1.add(label);
    }

}
