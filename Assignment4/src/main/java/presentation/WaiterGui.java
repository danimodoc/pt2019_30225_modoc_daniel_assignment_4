package presentation;

import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.ViewStart;

public class WaiterGui extends JFrame {

	JFrame f = new JFrame();
	private JButton newOrder_btn = new JButton("New Order");
	private JButton menu_btn = new JButton("Add Menu to Order");
	private JButton price_btn = new JButton("Compute  Price");
	private JButton genBill_btn = new JButton("Generate Bill");
	private JButton view_btn = new JButton("View orders");
	private JButton back = new JButton("Back");

	private JLabel l1 = new JLabel("Date");
	private JLabel l2 = new JLabel("Table");
	private JTextField lt1 = new JTextField(5);
	private JTextField lt2 = new JTextField(5);
	private JLabel l3 = new JLabel("Menu id");
	private JTextField lt3 = new JTextField(5);
	private JLabel l4 = new JLabel("Order id");
	private JTextField lt4 = new JTextField(5);
	
	public String[] columns = {"Number","Name","Price","Menu"};
	JScrollPane scrollPanel =new JScrollPane();
	
	private JTable orders = new JTable();

	public static int n = 0;

	public WaiterGui() {
		JPanel content1 = new JPanel();
		JPanel content12 = new JPanel();
		JPanel content2 = new JPanel();

		content2.add(newOrder_btn);

		content1.add(l1);
		content1.add(lt1);
		content1.add(l2);
		content1.add(lt2);
		
		content12.add(l3);
		content12.add(lt3);
		
		content12.add(l4);
		content12.add(lt4);
		content1.setLayout(new FlowLayout());

		content2.add(menu_btn);
		content2.add(price_btn);
		content2.add(view_btn);

		JPanel content3 = new JPanel();
		content2.add(genBill_btn);
		content2.setLayout(new FlowLayout());
		
		content12.setLayout(new FlowLayout());

		JPanel content4 = new JPanel();
		content4.add(back);

		JPanel content = new JPanel();
		content.add(content1);
		content.add(content12);
		content.add(content2);
		content.add(content3);
		content.add(content4);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 300);

		this.setContentPane(content);
		// f.pack();
		this.setTitle("Admin");
		this.setVisible(true);
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

		newOrder_btn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				newOrder_btnActionPerformed(evt);
			}
		});

		price_btn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				price_btnActionPerformed(evt);
			}
		});

		genBill_btn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				genBill_btnActionPerformed(evt);
			}
		});
		
		view_btn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				view_btnActionPerformed(evt);
			}
		});
		
		menu_btn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				menu_btnActionPerformed(evt);
			}
		});


		back.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				back_btnActionPerformed(evt);
			}
		});

	}

	private void newOrder_btnActionPerformed(java.awt.event.ActionEvent evt) {
		try {
			ViewStart.getRestaurant().addOrder(new SimpleDateFormat("dd/mm/yyyy").parse(lt1.getText()), Integer.parseInt(lt2.getText()));
			JOptionPane.showMessageDialog(null,"Order created");
			
		} catch (NumberFormatException e) {
			// todo auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// todo auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void menu_btnActionPerformed(java.awt.event.ActionEvent evt) {
		
		for(Order o:ViewStart.getRestaurant().getList().keySet()) {
			 
				try {
					if(o.getOrderId() == Integer.parseInt(lt4.getText())) {
				        ViewStart.getRestaurant().addMenuToOrder(o, Integer.parseInt(lt3.getText()));
				        
				        ChefGui ob = new ChefGui();
				        String s=null;
				        
				        ViewStart.getRestaurant().addObserver(ob);
				        
				        for(MenuItem m:ViewStart.getRestaurant().getMenuList()) {
				        	if(Integer.parseInt(lt3.getText()) == m.getId())
				        	{
				               s = "Order came to cook or prepare :"+m.getName();
				               break;
				        	}
				        }
				        ViewStart.getRestaurant().notifyChef(s);
				        
				        JOptionPane.showMessageDialog(null,"Added menu to order");
				        
				        ViewStart.getRestaurant().saveRestaurantInfo();
				        
				        ob.setVisible(true);
				        
				        break;
					}
				}
				catch(NumberFormatException e) {
					JOptionPane.showMessageDialog(null,"Please insert order id");
				}
			
		}
	}

	private void price_btnActionPerformed(java.awt.event.ActionEvent evt) {
		for(Order o:ViewStart.getRestaurant().getList().keySet()) {
			 
			try {
				if(o.getOrderId() == Integer.parseInt(lt4.getText())) {
			        double price=ViewStart.getRestaurant().orderPrice(o);
			        JOptionPane.showMessageDialog(null,"Total price for Order no."+o.getOrderId()+" is "+price+" RON");
			        break;
				}
			}
			catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null,"Please insert order id");
			}
		
	}
	}
	
	private void view_btnActionPerformed(java.awt.event.ActionEvent evt) {
		DefaultTableModel tableModel = new DefaultTableModel(columns,0);
		orders = new JTable(tableModel);
		
		JFrame newn = new JFrame();
        newn.setSize(400, 300);
        JPanel all = new JPanel();
        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        p2.add(new JLabel("Order Id      Date               Table                 Menu Id"));
        
        Iterator hmIterator = ViewStart.getRestaurant().getList().entrySet().iterator(); 
        int i=0;
        
        for (Entry<Order, ArrayList<MenuItem>> ee : ViewStart.getRestaurant().getList().entrySet()) {
        	
            Order key = ee.getKey();
            ArrayList<MenuItem> values = ee.getValue();
            
            String s="";
            
            for(int j=0;j<values.size();j++)
            	s=s+values.get(j).getId()+",";
            
            Object[] obs = {key.getOrderId(),key.getDate(),key.getTable(),s} ;//
            tableModel.addRow(obs);
            i++;
            // TODO: Do something.
            
        }
        
        i=0;
	
		orders.setModel(tableModel);
		
		p1.add(scrollPanel);
		p1.add(orders);
		all.add(p2);
		all.add(p1);
		all.setLayout(new BoxLayout(all,BoxLayout.Y_AXIS));
		newn.setContentPane(all);
		newn.pack();
		newn.setVisible(true);
	}

	private void genBill_btnActionPerformed(java.awt.event.ActionEvent evt) {
		for(Order o:ViewStart.getRestaurant().getList().keySet()) {
			 
			try {
				if(o.getOrderId() == Integer.parseInt(lt4.getText())) {
			        ViewStart.getRestaurant().generateBill(o);
			        JOptionPane.showMessageDialog(null,"Check generated");
			        break;
				}
			}
			catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null,"Please insert order id");
			}
		
	    }
	}

	private void back_btnActionPerformed(java.awt.event.ActionEvent evt) {
		ViewStart ob = new ViewStart();
		ob.f.setVisible(true);
		this.setVisible(false);
	}

}
