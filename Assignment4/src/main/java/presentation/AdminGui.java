package presentation;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import businessLayer.MenuItem;
import businessLayer.ViewStart;

public class AdminGui extends JFrame {
	
	JFrame f = new JFrame();
	private JLabel l1 = new JLabel("Name");
	private JLabel l2 = new JLabel("Price");
	private JTextField lt1 = new JTextField(5);
	private JTextField lt2 = new JTextField(5);
	
	private JButton newMenu_btn = new JButton("New Menu Item");
	private JButton deleteMenu_btn = new JButton("Delete Menu Item");
	private JButton editMenu_btn = new JButton("Edit Menu Item");
	private JButton viewMenu_btn = new JButton("View Menu Items");

	private JButton back = new JButton("Back");

	public String[] columns = {"Number","Name","Price"};
	JScrollPane scrollPanel =new JScrollPane();
	private JTable menus = new JTable();
	
	String[] col = {"Name","Price"};
	private int  id=-1;
	private int id2=0;
	
	public AdminGui() {
		JPanel content1 = new JPanel();
		content1.add(newMenu_btn);
		// content1.add(editMenu_btn);

		JPanel content2 = new JPanel();
		content2.setLayout(new FlowLayout());
		content2.add(l1);
		content2.add(lt1);
		content2.add(l2);
		content2.add(lt2);
		
		content1.add(deleteMenu_btn);
		
		JPanel content3= new JPanel();
		content1.add(editMenu_btn);
		content1.add(viewMenu_btn);
		content1.setLayout(new FlowLayout());
		
		JPanel content4= new JPanel();
		content4.add(back);

		JPanel content = new JPanel();
		content.add(content2);
		content.add(content1);
		//content.add(content3);
		content.add(content4);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 300);

		this.setContentPane(content);
		// f.pack();
		this.setTitle("Admin");
		this.setVisible(true);
		content.setLayout(new BoxLayout(content,BoxLayout.Y_AXIS));
		
		
		newMenu_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenu_btnActionPerformed(evt);
            }
        });
		
		deleteMenu_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteMenu_btnActionPerformed(evt);
            }
        });
		
		editMenu_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editMenu_btnActionPerformed(evt);
            }
        });
		
		viewMenu_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewMenu_btnActionPerformed(evt);
            }
        });
		
		back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                back_btnActionPerformed(evt);
            }
        });
		
	}
	
	private void newMenu_btnActionPerformed(java.awt.event.ActionEvent evt) {
		
		MenuItem menu = new MenuItem(++id,lt1.getText(),Integer.parseInt(lt2.getText()));
		ViewStart.getRestaurant().addMenu(menu);
		JOptionPane.showMessageDialog(null,"Menu item added");
	}
	
	private void deleteMenu_btnActionPerformed(java.awt.event.ActionEvent evt) {
		for(MenuItem m : ViewStart.getRestaurant().getMenuList()) {
			if(m.getName().equals(lt1.getText()))
			{
		     ViewStart.getRestaurant().getMenuList().remove(m);
		     JOptionPane.showMessageDialog(null,"Menu item deleted");
		     break;
			}
		}
	}
	
	private void editMenu_btnActionPerformed(java.awt.event.ActionEvent evt) {
		for(MenuItem m : ViewStart.getRestaurant().getMenuList()) {
			if(m.getName().equals(lt1.getText()))
			{
		     ViewStart.getRestaurant().editMenu(m,lt1.getText(),Integer.parseInt(lt2.getText()));
		     JOptionPane.showMessageDialog(null,"Menu item edited");
		     break;
			}
		}
	}
	
    private void viewMenu_btnActionPerformed(java.awt.event.ActionEvent evt) {
		DefaultTableModel tableModel = new DefaultTableModel(columns,0);
		menus = new JTable(tableModel);
		
		JFrame newn = new JFrame();
        newn.setSize(400, 300);
        JPanel all = new JPanel();
        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        p2.add(new JLabel("Id                Name              Price"));
        
        
		for(MenuItem m : ViewStart.getRestaurant().getMenuList()) {
			Object[] obs = {m.getId(),m.getName(),m.getPrice()} ;
			tableModel.addRow(obs);
		}
	
		menus.setModel(tableModel);
		
		p1.add(scrollPanel);
		p1.add(menus);
		all.add(p2);
		all.add(p1);
		all.setLayout(new BoxLayout(all,BoxLayout.Y_AXIS));
		newn.setContentPane(all);
		newn.pack();
		newn.setVisible(true);
	}
	
	private void back_btnActionPerformed(java.awt.event.ActionEvent evt) {
		ViewStart ob = new ViewStart();
		ob.f.setVisible(true);
		this.setVisible(false);
	}

}
