package businessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {
	
	private List<MenuItem> MenuList = new ArrayList<MenuItem>();
	
	public void addMenu(MenuItem menu) {
		MenuList.add(menu);
	}
	
	public void deleteMenu(MenuItem menu) {
		MenuList.remove(menu);
	}
	
	public void editMenu() {
		
	}
	
	public double computePrice() {
		
		double sum = 0;
		
		for(MenuItem m : MenuList) {
			sum = sum + m.computePrice();
		}
		
		return sum;
	}

}
