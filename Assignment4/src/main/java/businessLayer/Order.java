package businessLayer;

import java.io.Serializable;
import java.util.Date;

import javax.swing.JTable;

public class Order implements Serializable {

	private int orderId=0;
	private Date date;
	private int table;

	public Order(int orderIdo, Date date, int table) {
		super();
		this.orderId = orderIdo;
		this.date = date;
		this.table = table;
	}

	public Order() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + orderId;
		return result;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderIdo) {
		orderId = orderIdo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

}
