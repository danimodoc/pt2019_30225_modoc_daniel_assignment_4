package businessLayer;

import java.io.Serializable;

public class MenuItem implements Serializable,Observer {
	
	private int id;
	private String name;
	private double price;
	
	public MenuItem(int id,String name, double price) {
		this.id=id;
		this.name = name;
		this.price = price;
	}
	
	public MenuItem() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
    public  double computePrice() {
		return this.getPrice();
	}
    
    public String messageChef(String s) {
    	return s;
    }

	public void notifyChef() {
		// TODO Auto-generated method stub
		
	}

}
