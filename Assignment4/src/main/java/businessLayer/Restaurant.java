package businessLayer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Map.Entry;

public class Restaurant extends Observable implements RestaurantProcessing  {//
	
	private HashMap<Order,ArrayList<MenuItem>> List = new HashMap<Order,ArrayList<MenuItem>>();
	private ArrayList<MenuItem> menuList = new ArrayList<MenuItem>();
	
	private HashMap<MenuItem,Observer> observerMenus = new HashMap<MenuItem,Observer>();
	
	private final String restaurant_file = "RestaurantInfo.ser";
	
	private int n=0;
	
	public Restaurant() {
		
	}
	
	public HashMap<Order, ArrayList<MenuItem>> getList() {
		return List;
	}

	public void setList(HashMap<Order, ArrayList<MenuItem>> list) {
		List = list;
	}

	public ArrayList<MenuItem> getMenuList() {
		return menuList;
	}

	public void setMenuList(ArrayList<MenuItem> menuList) {
		this.menuList = menuList;
	}

	public int startId=0;
	
	/**
      Add menu into the menuList
      @precondition: Menu exists
      @postcondition: Menu was added
      @param menu
      @return void
    */
	
	public void addMenu(MenuItem menu) {
		assert menuList.contains(menu): "menuitem already in list";
		menuList.add(menu);
	}
	
	/**
    Edit menu from menuList
    @precondition: Menu exists
    @postcondition: Menu was edited
    @param menu
    @return void
  */

    public void editMenu(MenuItem menu,String name , int price) {
 
		for(MenuItem m : menuList) {
			if(m.getName().equals(name) && m.getPrice() == price)
			{
				m.setName(name);
				m.setPrice(price);
			}
		}
	}
    
    /**
    Delete menu from menuList
    @precondition: Menu exists
    @postcondition: Menu was edited
    @param menu
    @return void
  */

    public void deleteMenu(MenuItem menu) {
    	assert menuList.contains(menu):"The menu is not in the List";
    	menuList.remove(menu);
    }
    
    /**
     * Place an order in the List
     * @precondition: date and table have valid formats
     * @postcondition: order is inserted into the hashMap
     * @param Date date
     * @param int table : number of the table
     * @return void
     */
    
    public void addOrder(Date date,int table) {//int orderId,
    	
    	Order order = new Order(++n,date,table);
    	List.put(order,new ArrayList<MenuItem>());
    }
    
    /**
     * Associates a menuItem with an order
     * @precondition: order exists , menuId "exists"
     * @postcondition: menuItem from menuList is associated with an order
     * @param order
     * @param ,menuID
     * @return void
     */
    
    public void addMenuToOrder(Order order ,int menuId) {
    	assert List.keySet().contains(order):"Such order doesn't exist";
    	List.get(order).add(menuList.get(menuId));

    	for(MenuItem m: menuList) {
    		if(m.getId() == menuId) {
    			//observerMenus.
    			break;
    		}
    	}
    }
    
    /**
     * Calculates order price
     * @precondition: order exists
     * @postcondition: price is calculated
     * @param order
     * @return price
     */
    
    public double orderPrice(Order order) {
    	assert List.keySet().contains(order):"Such order doesn't exist";
    	
    	double sum = 0;
    	
    	for(MenuItem m : List.get(order)) {
    		sum = sum + m.computePrice();
    	}
    	
    	return sum;
    }
    
    /**
     * Generates a file with order details
     * @precondition: order exists
     * @postcondition: price is calculated
     * @param order
     * @return price
     */
    public void generateBill(Order order) {
    	PrintWriter writer;
		try {
			writer = new PrintWriter("Order"+order.getOrderId()+".txt", "UTF-8");
			
			writer.println("Order no."+order.getOrderId());
			
			for(MenuItem m : List.get(order)) {
				writer.println(m.getName()+" "+m.getPrice());
	    	}
			
	    	writer.println("Total due : " + orderPrice(order));
	    	writer.close();
	    	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    public int generateOrderId() {
    	return ++startId;
    }
    
    /**
     * Saves restaurant info through serialization into a file 
     * @precondition: 
     * @postcondition: file is populated with serialized objects
     * @param void
     * @return void
     */
    
    public void saveRestaurantInfo() {
    	FileOutputStream file;
		try {
			file = new FileOutputStream(restaurant_file);
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(List);
			out.writeObject(menuList);
			out.writeObject(n);
			out.close();
			
		} catch (IOException e) {

			e.printStackTrace();
		} 
    }
    
    
    
    public boolean wellFormedState() {
    	
    	if(menuList == null || List == null)
    		return false;
    	
    	return true;
    }
    
    public boolean wellFormedStateElements() {
    	
	for (Entry<Order, ArrayList<MenuItem>> ee : List.entrySet()) {
       if(ee.getKey() == null && ee.getValue() == null )
    	   return false;
    }
    	return true;
    }
    
    /**
     * Loads restaurant info through serialization from a file 
     * @precondition: 
     * @postcondition: info is retracted from file
     * @param void
     * @return void
     */
    
    public void loadRestaurantInfo() {
    	
    	FileInputStream file;
		try {
			
			file = new FileInputStream(restaurant_file);
			ObjectInputStream out = new ObjectInputStream(file);
			
			List = (HashMap<Order,ArrayList<MenuItem>>)out.readObject();
			menuList = (ArrayList<MenuItem>)out.readObject();
			n=(Integer)out.readObject();
			out.close();
			
		} catch (IOException e) {

			e.printStackTrace();
		} 
		catch(ClassNotFoundException cnfe){
            //cnfe.printStackTrace();
        }
    }
    
    public void notifyChef(String s) {
    	setChanged();
    	notifyObservers(s);
    }
}
