package businessLayer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import presentation.AdminGui;
import businessLayer.Restaurant;
import presentation.WaiterGui;


public class ViewStart {
	
	public JFrame f = new JFrame();
	private JButton admin_btn = new JButton("Log in as Admin");
	private JButton waiter_btn = new JButton("Log in as Waiter");
	private JButton chef_btn = new JButton("Log in as Chef");

	static Restaurant restaurant = new Restaurant();
	
	public ViewStart() 
	{
		restaurant.loadRestaurantInfo();
		JPanel content1 = new JPanel();
		content1.add(admin_btn);
		// content1.add(products_btn);

		JPanel content2 = new JPanel();
		content2.add(waiter_btn);
		
		JPanel content3= new JPanel();
		content3.add(chef_btn);

		JPanel content = new JPanel();
		content.add(content1);
		content.add(content2);
		content.add(content3);

		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(600, 300);

		f.setContentPane(content);
		// f.pack();
		f.setTitle("Restaurant App");
		f.setVisible(true);
		content.setLayout(new BoxLayout(content,BoxLayout.Y_AXIS));
		
		admin_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                admin_btnActionPerformed(evt);
            }
        });
		
		waiter_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                waiter_btnActionPerformed(evt);
            }
        });
		
		chef_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chef_btnActionPerformed(evt);
            }
        });
	}
	
	public static Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		ViewStart.restaurant = restaurant;
	}

	private void admin_btnActionPerformed(java.awt.event.ActionEvent evt) {
		
		AdminGui ob = new AdminGui();
		f.setVisible(false);
		
	}
	
	private void waiter_btnActionPerformed(java.awt.event.ActionEvent evt) {
		
		WaiterGui ob = new WaiterGui();
		f.setVisible(false);
		
	}
	
	private void chef_btnActionPerformed(java.awt.event.ActionEvent evt) {
		
	}

}
