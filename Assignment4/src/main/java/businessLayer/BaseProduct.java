package businessLayer;

public class BaseProduct extends MenuItem {

	private int id;
	private String name;
	private double price;

	public BaseProduct(int id, String name, double price) {
		super(id,name,price);
		this.id=id;
		this.name = name;
		this.price = price;
	}

	public BaseProduct() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double computePrice() {
		return price;
	}

}
