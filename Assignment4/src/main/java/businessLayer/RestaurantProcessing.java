package businessLayer;

import java.util.Date;

public interface RestaurantProcessing {
	
	public void addMenu(MenuItem menu);
	public void editMenu(MenuItem menu,String name , int price);
	public void deleteMenu(MenuItem menu);
	
	public void addOrder(Date date,int table);
	public void addMenuToOrder(Order order ,int menuId);
	public double orderPrice(Order order);
	public void generateBill(Order order);

}
